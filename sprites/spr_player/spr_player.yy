{
    "id": "6fbae490-f059-4d11-9eba-519f4c386fe9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 15,
    "bbox_right": 33,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c5ddffdf-6756-4697-889e-2738b01df40f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fbae490-f059-4d11-9eba-519f4c386fe9",
            "compositeImage": {
                "id": "830be85c-d5f1-4126-81e2-2350832b5c25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5ddffdf-6756-4697-889e-2738b01df40f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6dd77f1-c79c-4ba8-9c0e-b02e9a219cb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5ddffdf-6756-4697-889e-2738b01df40f",
                    "LayerId": "2545854d-7000-4260-b8e3-f480c9a52231"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "2545854d-7000-4260-b8e3-f480c9a52231",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fbae490-f059-4d11-9eba-519f4c386fe9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}