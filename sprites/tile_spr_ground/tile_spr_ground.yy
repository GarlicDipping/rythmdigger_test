{
    "id": "1c6f492b-6f4c-42fa-9cfa-3fd8e8409132",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tile_spr_ground",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3dfc0f85-029f-42a5-aace-666a9b35877e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c6f492b-6f4c-42fa-9cfa-3fd8e8409132",
            "compositeImage": {
                "id": "466adb94-d64a-4e40-a136-f61a4716a12a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dfc0f85-029f-42a5-aace-666a9b35877e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdb1a170-dcd8-4b7d-87e3-4f9ddd55dc19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dfc0f85-029f-42a5-aace-666a9b35877e",
                    "LayerId": "7f240d4c-e16d-4d2f-b541-08d3d99afc55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7f240d4c-e16d-4d2f-b541-08d3d99afc55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c6f492b-6f4c-42fa-9cfa-3fd8e8409132",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}