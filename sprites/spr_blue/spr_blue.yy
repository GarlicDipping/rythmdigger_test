{
    "id": "85253003-a86a-4b83-91e1-05bf785aabd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4d2f1db7-88c1-4c59-8d2b-083a12e7b94a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "bb6ef254-9285-407c-a096-269d5864ca24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d2f1db7-88c1-4c59-8d2b-083a12e7b94a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42917ecc-5fab-4d00-8423-e49f296d6b61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d2f1db7-88c1-4c59-8d2b-083a12e7b94a",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        },
        {
            "id": "dfa62622-0321-4cb6-925d-829f55a195b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "5d6432d7-71c5-4913-8ad0-d7a11197dc52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa62622-0321-4cb6-925d-829f55a195b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59ded4e5-d3b4-401a-836d-a7136b805353",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa62622-0321-4cb6-925d-829f55a195b1",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        },
        {
            "id": "e241662c-11cc-4f82-85d3-c66500d2dc92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "375ea0b2-9df2-4c28-9739-25dfc17a3973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e241662c-11cc-4f82-85d3-c66500d2dc92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee7d2c00-9955-4b6f-aefd-aea484f2c9e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e241662c-11cc-4f82-85d3-c66500d2dc92",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        },
        {
            "id": "8cb4ac3c-836f-45be-a362-f633044e9b4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "fdd338c8-fa4f-4533-95ca-bac6e41ca0a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cb4ac3c-836f-45be-a362-f633044e9b4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22844e18-c188-4bfe-88e5-97e915455aec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cb4ac3c-836f-45be-a362-f633044e9b4f",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        },
        {
            "id": "fa7cb336-47da-49e5-8fd4-271b71c64471",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "423a7f23-3cc7-4e24-8633-d86532a51168",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa7cb336-47da-49e5-8fd4-271b71c64471",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d4d44ce-98ef-4e97-9f1d-7b681a184a9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa7cb336-47da-49e5-8fd4-271b71c64471",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        },
        {
            "id": "c03dd6f1-f17f-4e3c-b3eb-4aa8ed5c50d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "97e16273-29ef-4761-96c4-7e4f273a8845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c03dd6f1-f17f-4e3c-b3eb-4aa8ed5c50d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a00ab55a-6210-4ab4-933d-f81a7dfdf8e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c03dd6f1-f17f-4e3c-b3eb-4aa8ed5c50d5",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        },
        {
            "id": "79c61542-778d-48da-a322-19a9f8e16635",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "bec52b07-577a-4ba5-ad12-d67770443521",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79c61542-778d-48da-a322-19a9f8e16635",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea35631e-3b59-42cc-8f1b-1fcdef9a29c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79c61542-778d-48da-a322-19a9f8e16635",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        },
        {
            "id": "6d5b85bb-eaf2-456d-a3ea-207dd7529ef2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "c33ec66f-6aff-4858-8e56-d3919c43c395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d5b85bb-eaf2-456d-a3ea-207dd7529ef2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20e72689-d635-47b3-b510-e6248762926a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d5b85bb-eaf2-456d-a3ea-207dd7529ef2",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        },
        {
            "id": "01daca1c-495d-4c59-b8e6-7fb9128f0164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "91159f40-3f52-4d5b-8ead-a0c62ca25cf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01daca1c-495d-4c59-b8e6-7fb9128f0164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71f11f60-2687-4d31-ac75-a2259c02147f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01daca1c-495d-4c59-b8e6-7fb9128f0164",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        },
        {
            "id": "fb0a8492-6384-4279-a54e-66b687f0c3bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "f3a96f0d-7c58-4180-bdb5-b5d270847b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb0a8492-6384-4279-a54e-66b687f0c3bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68859924-1aef-4e52-8b55-46e2b7a96178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb0a8492-6384-4279-a54e-66b687f0c3bf",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        },
        {
            "id": "b0a455b8-c32e-41ba-b10d-4e44a8fc9f36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "aa3a565c-dc1d-4284-9d33-13e6db88e578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0a455b8-c32e-41ba-b10d-4e44a8fc9f36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a975e353-4905-4dac-a44e-b38d04f6dbd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0a455b8-c32e-41ba-b10d-4e44a8fc9f36",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        },
        {
            "id": "1a689ff8-3f6a-4d40-84d7-52b543ccfb44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "compositeImage": {
                "id": "5068aed9-b8f6-449b-bf7a-83ade9d10619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a689ff8-3f6a-4d40-84d7-52b543ccfb44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "657238e1-ebdd-4e69-820e-6c947f5ca03d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a689ff8-3f6a-4d40-84d7-52b543ccfb44",
                    "LayerId": "2acee6bb-e08e-430f-ba00-02ba69875a16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2acee6bb-e08e-430f-ba00-02ba69875a16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85253003-a86a-4b83-91e1-05bf785aabd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}